#!/bin/bash

mainmenu() {
    echo -ne "
Bash!
2) Вывести информацию о том, какое ядро используется данным дистрибутивом
4) Вывести список всех разделов
6) Посмотреть сколько занимают места все файлы и папки в домашней директории пользователя и сортировать его по возрастанию
8) Включить отладку в скрипте
10) Дополнить скрипт, чтобы он разделял слова “Hello” и “World” с помощью ASCII-символа
0) Exit
Choose an option:  "
    read -r ans
    case $ans in
    2)
        uname -r
        ;;
    4)
        df -h
        ;;
    6)
        submenu
        mainmenu
        ;;
    8)
        submenu
        mainmenu
        ;;
    10)
        submenu
        mainmenu
        ;;
    1)
        submenu
        mainmenu
        ;;
    *)
        echo "Wrong option."
        exit 1
        ;;
    esac
}

mainmenu